app {
  url    = "https://gitlab.com/fadhilma/nomad-pack"
  author = "Fadhil"
}

pack {
  name        = "example_nomad_postgresql"
  description = ""
  url         = "https://gitlab.com/fadhilma/nomad-pack/-/tree/main/packs/example-nomad-postgresql"
  version     = "0.0.1"
}