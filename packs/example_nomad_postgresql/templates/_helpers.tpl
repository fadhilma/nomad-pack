// allow nomad-pack to set the job name

[[- define "job_name" -]]
[[- if eq .example_nomad_postgresql.job_name "" -]]
[[- .example_nomad_postgresql.pack.name | quote -]]
[[- else -]]
[[- .example_nomad_postgresql.job_name | quote -]]
[[- end -]]
[[- end -]]

// only deploys to a region if specified

[[- define "region" -]]
[[- if not (eq .example_nomad_postgresql.region "") -]]
region = [[ .example_nomad_postgresql.region | quote]]
[[- end -]]
[[- end -]]