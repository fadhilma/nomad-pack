job "[[.example_nomad_postgresql.job_name]]" {

  datacenters = [[.example_nomad_postgresql.datacenters]]

  type = "[[.example_nomad_postgresql.type]]"

  update {
    max_parallel = [[.example_nomad_postgresql.update_max_parallel]]
    min_healthy_time = "[[.example_nomad_postgresql.update_min_healthy_time]]"
    healthy_deadline = "[[.example_nomad_postgresql.update_healthy_deadline]]"
    auto_revert = [[.example_nomad_postgresql.update_auto_revert]]
    canary = [[.example_nomad_postgresql.update_canary]]
  }

  group "[[.example_nomad_postgresql.job_name]]" {
    count = [[.example_nomad_postgresql.count]]

    service {
      name = "[[.example_nomad_postgresql.service_name]]"
      tags = ["postgres for vault"]
      port = "[[.example_nomad_postgresql.service_port]]"

      check {
        name     = "[[.example_nomad_postgresql.service_check_name]]"
        type     = "[[.example_nomad_postgresql.service_check_type]]"
        interval = "[[.example_nomad_postgresql.service_check_interval]]"
        timeout  = "[[.example_nomad_postgresql.service_check_timeout]]"
      }
    }

    task "[[.example_nomad_postgresql.job_name]]" {
      driver = "[[.example_nomad_postgresql.driver]]"

      config {
        image = "[[.example_nomad_postgresql.config_image]]"
        network_mode = "[[.example_nomad_postgresql.config_network_mode]]"
        port_map {
            db = [[.example_nomad_postgresql.config_port_map]]
        }
      }
      

      env {
        POSTGRES_USER="[[.example_nomad_postgresql.env_POSTGRES_USER]]"
        POSTGRES_PASSWORD="[[.example_nomad_postgresql.env_POSTGRES_PASSWORD]]"
      }

      resources {
        cpu = [[.example_nomad_postgresql.resources.cpu]]
        memory = [[.example_nomad_postgresql.resources.memory]]
        network {
          mbits = [[.example_nomad_postgresql.resouce_network_mbits]]
          port  "db"  {
            static = [[.example_nomad_postgresql.resouce_network_port_db]]
          }
        }
      }

      restart {
        attempts = [[.example_nomad_postgresql.restart_attempts]]
        interval = "[[.example_nomad_postgresql.restart_interval]]"
        delay = "[[.example_nomad_postgresql.restart_delay]]"
        mode = "[[.example_nomad_postgresql.restart_mode]]"
      }

    }
  }
}
