Congrats on deploying [[ .example_nomad_postgresql.job_name ]].

There are [[ .example_nomad_postgresql.job_name ]] instances of your job now running on Nomad.